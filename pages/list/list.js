// pages/list/list.js
const app = getApp();
Page({
  data: {
    msg: [],
    hosts:'',
    canIUse:wx.canIUse('button.open-type.getUserInfo'),
    isAuth:app.globalData.isAuth
  },
  onLoad: function () {
    let that = this;
    
    app.userInfoReadyCallback = function (res) {
      that.getExhibition();
    };

  },
  getExhibition: function () {
    let that = this;
    wx.request({
      url: app.globalData.hosts + '/index/exhibition/index',
      header: {
        'X-Requested-With': 'XMLHttpRequest',
        'authentication': app.globalData.token
      },
      success: function (res) {
        let msg = JSON.parse(res.data.msg);
        that.setData({ msg: msg, hosts: app.globalData.hosts });
      },
    });
  }
});