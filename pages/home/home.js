const app = getApp();
Page({
  data:{
    msg:[],
    
  },
  onLoad:function(){
    //console.log('home onload');
    let that = this;
    app.userInfoReadyCallback = function (res) {
      that.getExhibition();
    };
    
  },
  onGotUserInfo: function (e) {
    console.log(e.detail.errMsg)
    console.log(e.detail.userInfo)
    console.log(e.detail.rawData)
  },
  getExhibition:function(){
    let that = this;
    wx.request({
      url: app.globalData.hosts + '/index/exhibition/index',
      header: {
        'X-Requested-With': 'XMLHttpRequest',
        'authentication': app.globalData.token
      },
      success: function (res) {
        //console.log('res.data.msg');
        //console.log(res.data.msg);
        let msg = JSON.parse(res.data.msg);
        that.setData({ msg: msg, hosts: app.globalData.hosts });
      },
    });
  }
});