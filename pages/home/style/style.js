// pages/home/style/style.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:0,

    hosts:app.globalData.hosts,
    image_list:[],
    marginTop:'141rpx'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options);
    let that = this;
    console.log(wx.getSystemInfoSync());
    this.setData({
      id:options.id,
      marginTop:'10rpx'
    });

    wx.request({
      url: app.globalData.hosts + '/index/api/getStyle',
      header:{
        'X-Requested-With':'XMLHttpRequest',
        'authentication':app.globalData.token
      },
      data:{
        id:this.data.id
      },
      success:function(res){
        console.log(res.data.msg);
        that.setData({
          image_list:res.data.msg
        });
      }
    })
  },

  goback:function(){
    wx.navigateBack({
      
    });
  }
 
})