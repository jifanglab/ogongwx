const Promise = require('../../../lib/promiseEs6Fix')
const app = getApp();
const WxParse = require('../../../wxParse/wxParse.js');

Page({
  data: {
    /**域名 */
    hosts: '',

    /**地区数据 */
    province: ['美国', '中国', '巴西', '日本','随便来一个'],
    city:[],
    district:[],
    curProvinceId:0,
    curProvinceName:'未知',
    curCityId:0,
    curCityName:'未知',
    curDistrict:0,
    curDistrictName:'未知',

    /**接受页面传递参数 */
    id:0,  //活动id
    
  },
  onLoad:function(options){
    let that = this;
    wx.request({
      url: app.globalData.hosts + '/index/exhibition/detail/id/'+options.id,
      header:{
        'X-Requested-With':'XMLHttpRequest',
        'authentication': app.globalData.token
      },
      data:{},
      method:'GET',
      success:function(res){
        console.log(res);
        WxParse.wxParse('article', 'html', res.data.msg, that, 5);
      }
    });

    this.getProvinces();
    this.setData({
      id:options.id //详情id 
    });
  },
  getProvinces:function(){
    let that = this;
    wx.request({
      url: app.globalData.hosts + '/index/api/getProvince',
      header: {
        'X-Requested-With': 'XMLHttpRequest',
        'authentication': app.globalData.token
      },
      success: function (res) {
        res = JSON.parse(res.data.msg);
        console.log(res.data);
        that.setData({
          province:res.data,
          curProvinceName:res.data[0].name,
          curProvinceId:res.data[0].id
        });
        that.getCities(res.data[0].id);
      }
    });
  },
  getCities:function(provinceId){
     let that = this;
     wx.request({
       url: app.globalData.hosts + '/index/api/getCities',
       header:{
         'X-Requested-With':'XMLHttpRequest',
         'authentication':app.globalData.token
       },
       data:{
         provinceId:provinceId
       },
       success:function(res){
         res = JSON.parse(res.data.msg);
         if(res.status){
           that.setData({
             city: res.data,
             curCityName: res.data[0].name,
             curCityId: res.data[0].id
           });
           that.getDistrict(res.data[0].id);
         }else{
           //没有数据，设置默认值 默认-1 比较好 因为为空的时候 也会变成0 不易辨别  0表示根本没选  -1表示选了 但是没有值  操作过快就会产生0
           that.setData({
             curCityName: '当前省没有下级数据（可以不选）',
             curCityId: '-1'
           });
           that.getDistrict('-1');
         }
         console.log({"position:":"获取城市","res":res});
       },
     });
  },
  getDistrict:function(cityId){
    let that = this;
    wx.request({
      url: app.globalData.hosts + '/index/api/getDistricts',
      header: {
        'X-Requested-With': 'XMLHttpRequest',
        'authentication': app.globalData.token
      },
      data: {
        cityId: cityId
      },
      success: function (res) {
        res = JSON.parse(res.data.msg);
        if(res.status){
          console.log(res.data);
          that.setData({
            district: res.data,
            curDistrictName: res.data[0].name,
            curDistrictId: res.data[0].id
          });
          that.getCompany(res.data[0].id);
        }else{
          that.setData({
            curDistrictName: '当前市没有下级数据（可以不选）',
            curDistrictId: '-1'
          });
        }
      },
    
    });
  },
  //要保证操作到这一步 不然不允许跳转 
  getCompany:function(districtId){
    let that = this;
    wx.request({
      url: app.globalData.hosts + '/index/api/getCompany',
      header: {
        'X-Requested-With': 'XMLHttpRequest',
        'authentication': app.globalData.token
      },
      data: {
        districtId: districtId
      },
      success:function(res){
        let data = JSON.parse(res.data.msg);
        if(data.status){

        }else{

        }
      }
    })
  },
  bindProvinceChange: function (e) {
    let that = this;
    console.log('picker发送选择改变，携带值为', e.detail.value)
    //console.log(that.data."4"
    let item = this.data.province[e.detail.value];
    this.setData({
      provinceId: e.detail.value,
      curProvinceName: item.name,
      curProvinceId: item.id
    });
    this.getCities(item.id);
  },
  bindCityChange:function(e){
     if(e.detail.value == 0){
        return;  
     }
     
     console.log(e.detail.value);
     let item = this.data.city[e.detail.value];
     this.setData({
       curCityName: item.name,
       curCityId: item.id
     });
     this.getDistrict(item.id);
  },
  bindDistrictChange:function(e){
    if (e.detail.value == 0) {
      return;
    }
     let item = this.data.district[e.detail.value];
     this.setData({
       curDistrictName: item.name,
       curDistrictId: item.id
     });
  },
  formSubmit:function(e){
    console.log("报名下一步");
    //报名的数据还是先保留在本地 然后一次性提交
    //选择用一次性提交 ，因为如果分开提交，还要处理用户第二次进来的时候 数据的显示问题 一次性提交 就让用户自己重新填写 如果退出的话
    //保存表单数据
    //这一步要验证用户的手机号码 然后再保存 当填写的手机号与本地存储的不一致说明有更改，仍然需要验证
    //短信验证最后加上去
    //已经报名的 这里不能再提交了  只有进入个人中心去修改了
    let formData = e.detail.value;
    if (this.data.curCityId == -1){
      this.data.curCityName = '未知';
    }
    if (this.data.curDistrictId == -1){
      this.data.curDistrictName = '未知';
    }
    wx.setStorageSync('sign_data',{
      'province_id':this.data.curProvinceId,
      'province_name':this.data.curProvinceName,
      'city_id': this.data.curCityId,
      'city_name':this.data.curCityName,
      'district_id': this.data.curDistrictId,
      'district_name':this.data.curDistrictName,
      'mobile':formData.mobile,
      'real_name':formData.real_name, 
      'exhibition_id':this.data.id,
      'invite_code':formData.invite_code,
    });
    wx.navigateTo({
      url: '/pages/home/stepTwo/stepTwo',
    });
  }
});