// pages/home/stepTwo/stepTwo.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hosts:app.globalData.hosts,
    styles:[],

    selectedNumber:0, //标记当前选择的风格数量 

    need_recommand:0, //是否需要风格推荐 0 不需要 1 需要 默认不需要
    show_choose_window:0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this;
    wx.request({
      url: app.globalData.hosts + '/index/api/getStyles',
      header:{
        'X-Requested-With':'XMLHttpRequest',
        'authentication':app.globalData.token
      },
      success:function(res){
        let styles = res.data.data;
        for(let i = 0;i<styles.length;i++){
          styles[i].state = 0;
        }
        that.setData({
          styles:styles
        });
      }
    })
  },
  askQuestion: function () {
    this.setData({
      show_choose_window:1,
    });
  },
  writeQuestion:function(e){
    this.data.need_recommand = e.target.dataset.type;
    this.setData({
      show_choose_window:0
    });
    this.doSubmit();
  },
  doSubmit:function(e){
    if(this.data.selectedNumber <= 0){
      wx.showToast({
        title: '最少选择一个风格',
        icon:'none',
        mask:true
      });
    }
    //获取风格
    let selectedStyles = [];
    for(let i = 0;i < this.data.styles.length; i++ ){
      if(this.data.styles[i].state == 1){
        selectedStyles.push(this.data.styles[i].id);
      }
    }

    //归总数据
    let sign_data = wx.getStorageSync('sign_data');
    sign_data.style_ids = selectedStyles;

    //先提示是否需要设计方案



    wx.request({
      url: app.globalData.hosts + '/index/exhibition/enroll',
      header:{
        'X-Requested-With':'XMLHttpRequest',
        'authentication':app.globalData.token
      },
      method:'POST',
      data:sign_data,
      success:function(res){
        console.log(res);
        if(res.data.code == 1){
          wx.showToast({
            title: res.data.msg,
            icon:'success',
            mask:true
          })
        }else{
          wx.showToast({
            title: res.data.msg,
            mask: true,
            icon: 'none'
          });
        }
      }
    });


    /*wx.navigateTo({
      url: '/pages/home/success/success',
    })*/
  },
  
  chooseStyle:function(data){
    let index = data.currentTarget.dataset.index;
    let state = this.data.styles[index].state;
    let selectedNumber = this.data.selectedNumber;
    state = !state;
    if (state == 1) {
      selectedNumber++;
    } else {
      selectedNumber--;
    }
    
    if(selectedNumber > 3){
       wx.showToast({
         title: '最多只能选择3个',
         icon:'none',
         mask:true
       });
       return;
    }

    this.data.styles[index].state = state;
    this.setData({
      styles:this.data.styles,
      selectedNumber: selectedNumber
    })
  }
})