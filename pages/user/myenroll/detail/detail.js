// pages/user/myenroll/detail/detail.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:'',  //团购活动id
    data:[],
    hosts:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      let that = this;
      this.setData({
        id:options.id,
        hosts:app.globalData.hosts
      });
      wx.request({
        url: app.globalData.hosts + '/index/enroll/detail/id/'+options.id,
        header:{
          'X-Requested-With':'XMLHttpRequest',
          'authentication':app.globalData.token
        },
        method:'POST',
        success:function(res){
          that.setData({
            data:res.data.msg
          });
          console.log(res.data.msg);
        }
      })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})