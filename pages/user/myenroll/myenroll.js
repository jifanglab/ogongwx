// pages/user/myenroll/myenroll.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    exhibition:[
      { id: 1, title: "活动名称", exhibition_time: '2018-10-10' },
      { id: 1, title: "活动名称", exhibition_time: '2018-10-10' },
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this;
    wx.request({
      url: app.globalData.hosts + '/index/enroll/myEnroll',
      header:{
        'X-Requested-With':'XMLHttpRequest',
        'authentication':app.globalData.token
      },
      method:'POST',
      success:function(res){
        //console.log(res.data.msg);
        that.setData({
          exhibition:res.data.msg
        });
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})