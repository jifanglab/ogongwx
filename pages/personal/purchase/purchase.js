// pages/personal/purchase/purchase.js
const app = getApp();
Page({

  /**
    * 页面的初始数据
    */
  data: {
    host: '',
    userInfo: null,
    enroll: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this;
    wx.request({
      url: app.globalData.hosts + '/index/enroll/myEnroll',
      header: {
        'X-Requested-With': 'XMLHttpRequest',
        'authentication': app.globalData.token
      },
      method: 'POST',
      success: function (res) {
        console.log(res.data.data);
        console.log(res.data.data.length);
        that.setData({
          enroll: res.data.data
        });
      }
    });
  }
})