// pages/personal/busorder/busorder.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    host: '',
    userInfo: null,
    order_list: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this;
    this.setData({
      hosts: app.globalData.hosts
    });
    wx.request({
      url: app.globalData.hosts + '/index/store/order',
      header: {
        'X-Requested-With': 'XMLHttpRequest',
        'authentication': app.globalData.token
      },
      method: 'POST',
      success: function (res) {
        console.log(res.data.data);

        if (res.data.code == 0 && res.data.data['type'] == 'not_login') {
          wx.setStorageSync('company_type', '-1');
          wx.showToast({
            title: res.data.msg,
            success: function () {
              wx.switchTab({
                url: '/pages/personal/personal',
              });
            }
          });
        }

        that.setData({
          order_list: res.data.data
        });
      }
    });
  }
})
