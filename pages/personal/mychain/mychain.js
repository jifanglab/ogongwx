// pages/personal/mychain/mychain.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    chain_list:[]
  },

  
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this;
    wx.request({
      url: app.globalData.hosts + '/index/store/mychain',
      header: {
        'X-Requested-With': 'XMLHttpRequest',
        'authentication': app.globalData.token
      },
      method: 'POST',
      success: function (res) {
        console.log(res);
        if (res.data.code == 0 ) {
          if (res.data.data['type'] == 'not_login'){
            wx.setStorageSync('company_type', '-1');
            wx.showToast({
              title: res.data.msg,
              success: function () {
                wx.switchTab({
                  url: '/pages/personal/personal',
                });
              }
            });
          }else{
            wx.showToast({
              title: res.data.msg,
              icon:'none'
            })
          }
        }

        that.setData({
          chain_list: res.data.data.data //分页的这个多了一层
        });
      }
    });
  }
  
})