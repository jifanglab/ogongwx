// pages/personal/withdraw/withdraw.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    company_info:[],  
  },

  /**
    * 生命周期函数--监听页面加载
    */
  onLoad: function (options) {
    //获取可以体现多少金额
    let that = this;
    wx.request({
      url: app.globalData.hosts + '/index/store/companyInfo',
      header: {
        'X-Requested-With': 'XMLHttpRequest',
        'authentication': app.globalData.token
      },
      method: 'POST',
      success: function (res) {
        console.log(res);
        if (res.data.code == 0 && res.data.data['type'] == 'not_login') {
          wx.setStorageSync('company_type', '-1');
          wx.showToast({
            title: res.data.msg,
            success: function () {
              wx.switchTab({
                url: '/pages/personal/personal',
              });
            }
          });

        }
        that.setData({
          company_info: res.data.data
        });
      }
    });
  },

  formSubmit:function(e){
    let that = this;
    let data = e.detail.value;
    wx.request({
      url: app.globalData.hosts + '/index/store/applyWithdraw',
      header: {
        'X-Requested-With': 'XMLHttpRequest',
        'authentication': app.globalData.token
      },
      data:data,
      method:'POST',
      success: function (res) {
        console.log(res);
        if (res.data.code == 0 && res.data.data['type'] == 'not_login' ) {
            wx.setStorageSync('company_type', '-1');
            wx.showToast({
              title: res.data.msg,
              success: function () {
                wx.switchTab({
                  url: '/pages/personal/personal',
                });
              }
            });
          }else{
            wx.showToast({
              title: res.data.msg,
              icon:'none'
            });
        }
      }
    });
  }

})