// pages/personal/personal.js
const app = getApp();
const amapFile = require('../../lib/amap-wx.js');
Page({
  
  /**
   * 页面的初始数据
   */
  data: {
    hosts: '', 
    message:{
      hosts: 'https://yangjifang.yuanbaohua.top',
      isAuth:true,
      company: [],
    },
    template:-1,
    
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let that = this;
    let curData = {};
    curData.isAuth = app.globalData.isAuth;
    
    that.setData({
      message: curData,
    });

    let myAmapFun = new amapFile.AMapWX({ key: '57ebe9902f8a9beb751934d02c676c28' });
    myAmapFun.getRegeo({
      success: function (data) {
        console.log(data);
        //成功回调
      },
      fail: function (info) {
        //失败回调
        console.log(info)
      }
    });
  },


  onShow:function(){
    let that = this;
    //根据当前是哪个用户 显示对应信息
    let company_type = wx.getStorageSync('company_type');
    console.log(company_type);
    if (company_type !== undefined){
      this.setData({
        template:company_type
      });
    }
    
    let curData = {};
    curData.hosts = app.globalData.hosts;
    curData.userInfo = app.globalData.userInfo;
    curData.isAuth = app.globalData.isAuth;

    
    that.setData({
      hosts: app.globalData.hosts,
      message: curData,
      userInfo: app.globalData.userInfo
    });

  },

  /**
   * 商家登录
   */
  formSubmit:function(e){
      let value = e.detail.value;
      let that = this;
      wx.request({
        url: app.globalData.hosts + '/index/api/businessLogin',
        header: {
          'X-Requested-With': 'XMLHttpRequest',
          'authentication': app.globalData.token
        },
        method:'POST',
        data:value,
        success: function (res) {
          /**wx.showToast({
            title: '程序员正在加班升级...',
            icon:'none'
          });
          return;*/
          let data = res.data;
          wx.showToast({
            title: data.msg,
            icon:'none',
            success:function(){
              if (data.code == 1) {
                wx.setStorageSync('company_type', data.data['type']);
                wx.setStorageSync('company', data.data);
                let message = that.data.message;
                message.company = data.data;
                that.setData({
                  template:data.data['type'],
                  message:message
                });
              } 
            }
          })
          console.log(data.data['type']);
        },
      })
  },

  quit:function(){
    wx.setStorageSync('company_type', '-1');
    wx.showToast({
      title: '退出成功',
    });
    wx.switchTab({
      url: '/pages/list/list',
    })
  },

  onGotUserInfo: function (e) {
    let that = this;
    wx.getSetting({
      success: function (res) {
        if (res.authSetting['scope.userInfo']) {
          
          wx.getUserInfo({
            success: function (res) {
              // 可以将 res 发送给后台解码出 unionId
              app.globalData.userInfo = res.userInfo;
              app.globalData.isAuth = true;
              console.log({ userInfo: res.userInfo });
              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              wx.request({
                url: app.globalData.hosts + '/index/login/updateUserInfo',
                data: res.userInfo,
                method: 'POST',
                header: {
                  'X-Requested-With': 'XMLHttpRequest',
                  "Authentication": app.globalData.token
                },
                success: function (res) {
                  //console.log(res);
                }
              });

              let curData = that.data.message;
              curData.isAuth = true;
              curData.userInfo = res.userInfo;
              that.setData({
                message: curData
              });

            },
          });
        }
      }
    });

  }
})